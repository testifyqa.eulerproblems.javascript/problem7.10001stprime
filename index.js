function isPrime(numberToCheck) {
  let result = true;
  for(let i = 2; i <=(numberToCheck/i); i++) {
    if(numberToCheck%i == 0) { //if numberToCheck is divisible by number other than itself...
      result = false; //return false
    }
  }
  return result;
}

function getNthPrimeNumber(numberToCheck) {
  let count = 0;
  let prime = 1;

  while (count < numberToCheck) {
    prime++;
    if (isPrime(prime)) {
        count++;
    }
  }
  console.info("\nThe " + numberToCheck + "th prime number is: " + prime);

  return prime;
}

module.exports.getNthPrimeNumber = getNthPrimeNumber;