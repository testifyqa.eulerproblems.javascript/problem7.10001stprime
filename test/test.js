const assert = require('assert');
const index = require('../index.js');

describe('Problem3', function() {
  it('The 10001st prime number is correctly 104743', function() {
    assert.equal(index.getNthPrimeNumber(10001), 104743);
  });
});
